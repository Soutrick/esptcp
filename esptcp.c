// driver for esp8266 module

# include <stdio.h>
# include <string.h>
# include "platform.h"
#include "gpio_i2c.h"
#define LM75_SLAVE_ADDRESS 0x90
#define DELAY_COUNT 200
# define timeout 100
# define baud 115200

// purpose to use esp8266 as a client

#define UART_BASE_ADDRESS UART1_START

//Uart Status register
#define RECV_BUFF_NOT_EMPTY 8
#define RECV_BUFF_NOT_FULL 4
#define TX_BUFF_NOT_FULL 2
#define TX_DONE 1

//Following the UART memory map provided
#define UART_BAUD (UART_BASE_ADDRESS  + 0x00)
#define UART_TX (UART_BASE_ADDRESS  + 0x04)
#define UART_RX (UART_BASE_ADDRESS + 0X08)
#define UART_STATUS (UART_BASE_ADDRESS + 0x0C)


extern DelayLoop(long, long);

/*
macros to be used are :
UartTransmitString(char *s);
UartReceiveCharacter(void);
*/

// Hardcoding the pointers with addresss -- let's see if this works
short int* uart_baud = (const short int *) UART_BAUD;
char* uart_rx    = (const char *) UART_RX;
char* uart_tx  = UART_TX;
char* uart_status = (const char *) UART_STATUS;

char rcvBuf[200] = {'\0'};
char txBuf[200] = {'\0'};
char str[20];
int len = 0;

/*****************************LM75**********************************/
unsigned int Lm75ReadTemperature(unsigned long delay)
{
	unsigned char readBuf = 0;
	unsigned short readValue = 0;
	I2cSendSlaveAddress(LM75_SLAVE_ADDRESS, I2C_READ, delay);
	readBuf = I2cReadDataAck(delay);
	readValue = (readBuf << 1);
	readBuf = I2cReadDataNack(delay);
	I2cStop(delay);
	readValue = (readBuf >> 7) | readValue;
	return readValue;
}

/**********************************************************************/


void Uart1Init(short int baud1)
{
 *uart_baud = baud1;
}

//Used for DelayLoop
void delayLoop(int l ,int m)
{
 for (int i = 0;i<l;i++) for (int j=0;j<m;j++)
 continue;
}


//Used to transmit the character by UartTransmitCharacter
char * UartTransmitCharacter(char c)
{
 while(!(*uart_status & 0x2 ) );
 *uart_tx = c;
}

//Used to transmit the string by UartTransmitString
char * UartTransmitString(char * txStr)
{
 char stsRegRead = 0;
// printf("\nStart Transmit: ");
 while(*txStr != '\0')
 {
      while(!(*uart_status & TX_BUFF_NOT_FULL ) );
		  *uart_tx = *txStr++;
 }
}


int txCntrl = 0;
char ReceiveString(uint8_t *rcvString, unsigned long count)
{
     char rcvChar = '\0';
     char timeOut = 0;
     char stsRegRead = 0;
//     printf("\n Start Receive : ");
     while(1)
     {
    		//printf("\n Wait for receive");
    	 while(! ( (*uart_status) & RECV_BUFF_NOT_EMPTY ) );
    	 rcvChar = *uart_rx;
    		//printf("\n Rcv Data: %c", rcvChar);
    	 *rcvString++ = rcvChar;
//    		printf("\n Rcv String: %c", rcvChar);
        if (txCntrl == 0)
         {
           if(*(rcvString - 2) == 'O' && *(rcvString - 1) == 'K' )
             {
               *rcvString++ = '\n';
               *rcvString++ = '\r';
               *rcvString++ = '\0';
               printf("\tReceive Complete\n");
               return 1;
             }
        }
        else
        {
//          if(*(rcvString - 1) == '>' )
//printf("Entered in CIPSEND: %s\n", rcvString);
//          if( NULL != strchr(rcvString, '>')  )
            if(rcvChar == '>')
						{
//              printf("Entered in CIPSEND: %s\n", rcvString);
//              *rcvString++;
							//*rcvString++ = '\n';
//							*rcvString++ = '\r';
//							*rcvString++ = '\0';
	//						printf("\nNow Transmit can be done of specified Length");
							printf("Str. Length: %d", strlen(str));
              UartTransmitString(str);
							UartTransmitCharacter(0x1D);
							return 1;
						}
        }
     }
}

void SendAtCommand(char *txString , unsigned long count)
{
   char length = 5;
   printf("start send\n");
   UartTransmitString(txString);
   if(1 == txCntrl)
   {
     UartTransmitCharacter(length + 0x30);
     UartTransmitCharacter('\r');
     UartTransmitCharacter('\n');
   }
   if(1 == ReceiveString(rcvBuf, count) )
  	 printf("\nAT Response: %s\n",rcvBuf);
   else
  	 printf("\n Timedout When receiving Data.");
}

int connectionId = 49;

void init_client()
{
 printf("In init_client\n");

 memset(txBuf, '\0', 100);
 strcpy(txBuf, "AT\r\n"); // debug input
 printf("\n%s\n", txBuf);
 txCntrl = 0;
 SendAtCommand(txBuf, timeout);
// delayLoop(2000,2000);



 // memset(txBuf, '\0', 100);
 // strcpy(txBuf, "AT+RST\r\n"); // debug input
 // printf("\n%s\n", txBuf);
 // SendAtCommand(txBuf, timeout);
 // delayLoop(1000,1000);


 // memset(txBuf, '\0', 100);
 // strcpy(txBuf, "AT+CWMODE=1\r\n"); //softAP+station mode
 // printf("\n%s\n", txBuf);
 // txCntrl = 0;
 // SendAtCommand(txBuf,timeout);
 // delayLoop(1000,1000);


 memset(txBuf, '\0', 100);
 strcpy(txBuf, "AT+CIPSTART=\"TCP\",\"192.168.43.228\",8888\r\n");  // TCP send to specific IP and specific Port
 printf("\nTxBuf: %s\n", txBuf);
 txCntrl = 0;
 SendAtCommand(txBuf, timeout);
// delayLoop(2000,2000);


 memset(txBuf, '\0', 100);
 memset(rcvBuf, '\0', 100);
 //len = strlen(str);
 strcpy(txBuf, "AT+CIPSEND=");  // Send of Length 4 string
 txCntrl = 1;
 printf("\nTxBuf: %s\n", txBuf);
 SendAtCommand(txBuf, timeout);
// delayLoop(2000,2000);

 printf("--------------------------------------DONE----------------------------------------\n");
}


void main()
{
printf("\n----------------------------------------START-----------------------------------------------\n");
 Uart1Init( 50000000/(16 * baud)  ); // to initialize the baudrate of Uart1
 printf("In main\n");

 /*********************************LM75 main********************************************/
 unsigned short tempRead = 0;
 unsigned short temp = 0;
 // printf("\n\ttemp sensor initiating!");
 I2cInit();
 tempRead = Lm75ReadTemperature(DELAY_COUNT);
 printf("%u\n",tempRead );
 if(tempRead & 0x100)
 {
   tempRead = ( (~tempRead) + 1 ) >> 1;
   printf("\n\t Current Temperature:=-%u°C",tempRead);
 }
 else
  {
   tempRead = tempRead >> 1;
   str[0] = tempRead/10 + 0x30;
   str[1] = tempRead%10 + 0x30;
   str[2] = '\r';
   str[3] = '\n';
	 str[4] = '\0';
  printf("\t Current Temperature:=+%s°C\n",str);
  }
/***************************************************************************************/

 init_client();

 delayLoop(500000,500000);
}
